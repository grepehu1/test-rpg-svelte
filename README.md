# Test RPG Game in Svelte

This is a simple learning project in which I tried to make a RPG Maker style game using only Svelte.

Feel free to try it out, just install it with:

```
yarn install
```

And run the game with:

```
yarn dev
```

After that it'll be available in your browser at `http://localhost:5173/`.

Obviously you'll need to have installed in your computer NodeJS (more specifically v18 or greater) and yarn.